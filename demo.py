from tkinter import *
from random import randint
from tkinter import ttk
import psutil as ps

root = Tk()
root.title("Process your computer")
root.geometry("400x150")
lab = Label(root)
lab.pack()

RED = 'red.Horizontal.TProgressbar'
GREEN = 'green.Horizontal.TProgressbar'

def update():
   lab['text'] = randint(0,1000)
   root.after(1000, update) # run itself again after 1000 ms

def showPecent(x1,y1):
    txt_percent = ttk.Label(root,text = "%")
    txt_percent.place(x = x1, y = y1)

def ui():
    style = ttk.Style()
    style.theme_use('default')
    style.configure("red.Horizontal.TProgressbar", background='red')

    cpu_txt = ttk.Label(root,text="CPU: ")
    cpu_txt.place(x = 5,y=20)

    cpu_current = ttk.Progressbar(root,orient=HORIZONTAL,length=270,mode="determinate",value=50,style='red.Horizontal.TProgressbar')
    cpu_current.place(x = 50,y=20)

    cpu_percent = ttk.Label(root,text = "")
    cpu_percent.place(x = 340,y=20)

    showPecent(x1=370,y1=20)

    ram_txt = ttk.Label(root,text="RAM: ")
    ram_txt.place(x = 5,y=60)

    ram_current = ttk.Progressbar(root,orient=HORIZONTAL,length=270,mode="determinate",value=50)
    ram_current.place(x = 50,y=60)

    ram_percent = ttk.Label(root,text = "")
    ram_percent.place(x = 340,y = 60)

    showPecent(x1=370,y1=60)

    cpu = ps.cpu_percent()
    ram = ps.virtual_memory().percent
    if cpu and ram and cpu != cpu_current['value'] and ram != ram_current['value']:
        cpu_current['value'] = str(cpu) + " "
        cpu_percent['text'] = str(cpu)
        ram_current['value'] = str(ram) + " "
        ram_percent['text'] = str(ram)
        
        cpu_current['style'] = RED if cpu > 85 else GREEN
        ram_current['style'] = RED if ram > 70 else GREEN   
        
    #update process
    root.after(1000, ui)

ui()
#update()
root.mainloop()


#pip install ttk
#pip install psutil