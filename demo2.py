import tkinter as tk
from tkinter.ttk import Progressbar
from tkinter import ttk
canv = tk.Tk()
canv.title("Tkinter Progressbar")
canv.geometry('250x100')
style = ttk.Style()
style.theme_use('default')
style.configure("red.Horizontal.TProgressbar", background='red')
bar = Progressbar(canv, length=180, style='red.Horizontal.TProgressbar')
bar['value'] = 50
bar.grid(column=0, row=0)
canv.mainloop()